const fs = require('fs');
const path = require('path');
const util = require('util');
const request = require('request-promise-native');
const scale = require('scale-that-svg').default;
const spritezero = require('@mapbox/spritezero');
const generateImage = util.promisify(spritezero.generateImage);
const generateLayout = util.promisify(spritezero.generateLayout);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const ionicons = [
  'basket',
  'briefcase',
  'cart',
  'construct',
  'document',
  'leaf',
  'medkit',
  'pizza',
  'school',
  'videocam',
];

const platforms = [
  'ios',
  'md'
];

const external = [
  'https://maps.ccrpc.org/basemaps/basic/icons/oneway.svg'
];

const ionicons_path = 'node_modules/ionicons/dist/ionicons/svg/';
const spritesPath = 'src/assets/style/';


async function scaleIonicon(filename, size, src_dir) {
  const src_path = path.join(src_dir, filename);
  const input = await readFile(src_path);
  const scaled = await scale(input, {
    scale: size / 512
  });

  return {
    svg: Buffer.from(scaled),
    id: filename.replace('.svg', '')
  };
}

async function scaleIonicons(size) {
  let filenames = [];
  for (let ionicon of ionicons) {
    for (let platform of platforms) {
      filenames.push(`${platform}-${ionicon}.svg`);
    }
  }

  return await Promise.all(filenames.map(
    (filename) => scaleIonicon(filename, size, ionicons_path)));
}

async function loadExternalSvg(url) {
  const parts = url.split('/');
  const id = parts[parts.length - 1].replace('.svg', '');
  const svgBuffer = await request({
    url: url,
    encoding: null
  });
  return {
    svg: svgBuffer,
    id: id
  }
}

async function loadExternalSvgs() {
  return await Promise.all(external.map((url) => loadExternalSvg(url)));
}

async function writeSprites(svgs, pixelRatio, format, outDir) {
  const pixelPart = (pixelRatio > 1) ? `@${pixelRatio}x` : '';
  const outPath = path.join(outDir, `sprite${pixelPart}.${format}`);
  const layout = await generateLayout({
    imgs: svgs,
    pixelRatio: pixelRatio,
    format: format === 'json'
  });

  let result;
  if (format === 'json') {
    result = JSON.stringify(layout);
  } else {
    result = await generateImage(layout);
  }

  return await writeFile(outPath, result);
}

async function generateSprites() {
  const svgGroups = await Promise.all([
    scaleIonicons(12),
    loadExternalSvgs()
  ]);
  const svgs = [].concat.apply([], svgGroups);
  return await Promise.all([
    writeSprites(svgs, 1, 'json', spritesPath),
    writeSprites(svgs, 1, 'png', spritesPath),
    writeSprites(svgs, 2, 'json', spritesPath),
    writeSprites(svgs, 2, 'png', spritesPath)
  ]);
}

generateSprites();
