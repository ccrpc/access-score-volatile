export const phrases = {
  as: {
    app: {
      title: "Access Score",
      options: "Options",
      destinations: {
        art: "Arts and entertainment",
        grocery: "Grocery stores",
        health: "Health facilities",
        job: "Jobs",
        park: "Parks",
        public: "Public facilities",
        retail: "Retail stores",
        restaurant: "Restaurants",
        school: "Schools",
        service: "Services",
      },
      modes: {
        pedestrian: "Pedestrian",
        bicycle: "Bicycle",
        bus: "Bus",
        vehicle: "Vehicle",
      },
      groups: {
        destinations: "Destination Types",
        modes: "Modes of Transportation",
      },
      combined: "All Modes",
      stories: "Access Stories",
    },
    "help-button": {
      destinations: "Learn about destination types",
    },
    "info-button": {
      title: "Learn More",
    },
    "info-menu": {
      destination: "Destinations",
      intro: "Introduction",
      scenario: "Scenarios",
    },
    intro: {
      map: "Explore the Map",
      story: "Choose an Access Story",
      title: "Welcome to Access Score",
    },
    legend: {
      absolute: "Access Score",
      difference: "Access Score Difference",
      project: "Future Projects",
      title: "Legend",
    },
    scenario: {
      "baseline-2015": "2015 Baseline",
      "custom-2023": "Custom Scenario",
      "preferred-2045": "2045 Preferred",
      "preferred-hsr-2045": "2045 Preferred + HSR",
    },
    scenarios: {
      compare: "Compare to baseline",
      label: "Scenario",
      info: "Learn about the scenarios",
    },
    scores: {
      heading: "Scores",
      subheading: "Between %{start} and %{end}",
    },
    "static-modal": {
      close: "Close",
    },
    toggles: {
      all: "Enable or disable all",
    },
  },
};
