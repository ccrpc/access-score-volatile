import { h, Component, Listen, Prop, State } from '@stencil/core';
import { Mode } from '@ionic/core';
import { colorRamps, getToggles } from '../utils';


@Component({
  tag: 'as-style'
})
export class Style {
  destinations: HTMLAsToggleElement[];
  modes: HTMLAsToggleElement[];
  platform!: Mode;
  popup: HTMLGlPopupElement;

  @State() json: any;

  @Prop() baseUrl: string;

  componentWillLoad() {
    this.destinations = getToggles('destinations');
    this.modes = getToggles('modes');
    this.updateJson();
  }

  @Listen('asToggleChange', { target: 'body' })
  @Listen('asScenarioChange', { target: 'body' })
  togglechangeHandler() {
    this.updateJson();
  }

  @Listen('glStoryChange', {target: 'body'})
  closePopup() {
    if (this.popup) this.popup.removePopup();
  }

  getSources(scenarios: HTMLAsScenariosElement) {
    let scenario = scenarios.scenario;
    let scenarioName: string = scenarios.default;
    if (scenario) scenarioName = (scenario.difference && scenarios.compare) ?
      scenario.difference : scenario.id;
    return {
      'access-score': {
        'url': `https://maps.ccrpc.org${this.baseUrl}/${scenarioName}.json`,
        'type': 'vector'
      }
    };
  }

  getLayers(scenarios: HTMLAsScenariosElement) {
    let layers = [
      this.getSegmentLayer(scenarios),
      ...this.getProjectLayers(scenarios),
      ...this.getDestinationLayers()
    ];
    return layers;
  }

  getSegmentLayer(scenarios: HTMLAsScenariosElement) {
    let sum : (string | string[])[] = ['+'];
    let condition : (string | string[])[] = ['all'];
    this.modes.forEach((mode) => {
      if (mode.enabled)
        this.destinations.forEach((dest) => {
          if (dest.enabled) {
            let field = `${mode.value}_${dest.value}`;
            sum.push(['get', field]);
            condition.push(['has', field])
          }
        });
    });  
    let compare = (scenarios.scenario &&
      scenarios.scenario.difference && scenarios.compare);
    let lineColors = (compare) ? colorRamps.difference : colorRamps.absolute;

    return {
      id: 'segment',
      type: 'line',
      source: 'access-score',
      'source-layer': 'segment',
      minzoom: 0,
      maxzoom: 22,
      paint: {
        'line-color': [
          'case',
          (condition.length < 2) ? ['literal', false] : condition,
          [
            'interpolate',
            ['linear'],
            (sum.length < 2) ? ['literal', 0] : ['/', sum, sum.length - 1],
            ...lineColors
          ],
          '#dddddd'
        ],
        'line-width': [
          'interpolate',
          ['linear'],
          ['zoom'],
          10, 1,
          22, 5
        ]
      }
    };
  }

  getProjectLayers(scenarios: HTMLAsScenariosElement) {
    let notBaseline = (scenarios.scenario && scenarios.scenario.difference);
    if (!notBaseline) return [];

    return [
      {
        id: 'project',
        type: 'line',
        source: 'access-score',
        'source-layer': 'project',
        minzoom: 0,
        maxzoom: 22,
        paint: {
          'line-color': '#444444',
          'line-dasharray': [2, 2],
          'line-width': [
            'interpolate',
            ['linear'],
            ['zoom'],
            10, 1,
            22, 5
          ]
        }
      },
      {
        id: 'project-label',
        type: 'symbol',
        source: 'access-score',
        'source-layer': 'project',
        minzoom: 14,
        layout: {
          'symbol-placement': 'line',
          'text-field': '{name}',
          'text-font': [
            'Noto Sans Regular'
          ],
          'text-letter-spacing': 0.1,
          'text-size': {
            stops: [
              [
                10,
                8
              ],
              [
                20,
                14
              ]
            ]
          },
          'text-rotation-alignment': 'map'
        },
        paint: {
          'text-color': '#000',
          'text-halo-color': 'hsl(0, 0%, 100%)',
          'text-halo-width': 2
        }
      },
    ];
  }

  getDestinationLayers() {
    return this.destinations
      .filter((dest) => dest.enabled)
      .map((dest) => {
        return {
          'id': `destination-${dest.value}`,
          'type': 'symbol',
          'source': 'access-score',
          'source-layer': 'destination',
          'filter': [
            '==',
            ['get', 'type'],
            dest.value
          ],
          'minzoom': 14,
          'maxzoom': 22,
          'layout': {
            'icon-image': `${this.platform || 'md'}-${dest.icon}`,
            'text-anchor': 'top',
            'text-field': [
              'step',
              ['zoom'],
              '',
              16, ['get', 'name']
            ],
            'text-font': ['Noto Sans Regular'],
            'text-max-width': 8,
            'text-offset': [0, 0.5],
            'text-size': 11
          },
          'paint': {
            'text-color': '#666666',
            'text-halo-blur': 1,
            'text-halo-color': 'rgba(255,255,255,0.75)',
            'text-halo-width': 1
          }
        };
      });
  }

  updateJson() {
    let scenarios = document.querySelector('as-scenarios');
    this.json = {
      version: 8,
      sources: this.getSources(scenarios),
      sprite: `${this.baseUrl}/assets/style/sprite`,
      glyphs: 'https://maps.ccrpc.org/fonts/{fontstack}/{range}.pbf',
      layers: this.getLayers(scenarios)
    };
  }

  render() {
    return (
      <gl-style json={this.json}
        id="app"
        clickableLayers={['segment']}>
        <gl-popup component="as-scores" layers={['segment']}
          ref={(popup: HTMLGlPopupElement) => this.popup = popup}></gl-popup>
      </gl-style>
    );
  }
}
