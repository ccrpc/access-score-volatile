import { h, Component, Element } from '@stencil/core';
import { popoverController } from '@ionic/core';
import { _t } from '../../i18n/i18n';


@Component({
  tag: 'as-intro'
})
export class Intro {
  @Element() el: HTMLElement;

  async openPopover(e: Event) {
    const popover = await popoverController.create({
      component: 'as-story-list',
      event: e
    });

    await popover.present();
  }

  closeModal() {
    this.el.closest('ion-modal').dismiss();
  }

  render() {
    let snt = 'https://ccrpc.org/programs/sustainable-neighborhoods-toolkit/';
    let lrtp = 'https://ccrpc.gitlab.io/lrtp2045/';
    return ([
      <as-modal name={_t('as.intro.title')}>
        <p>
          Access Score displays data from
          the <a href={snt}>Sustainable Neighborhoods Toolkit</a> about
          neighborhood-level accessibility in Champaign County. It answers
          questions about how easy it is to reach common destinations
          using four modes of transportation: pedestrian, bicycle, bus, and
          vehicle.
        </p>
        <p>
          To explore the accessibility map, first select a scenario
          using the menu at the left. You can view existing conditions
          in the 2015 Baseline scenario, or choose a future scenario
          from the <a href={lrtp}>2045 Long Range Transportation Plan</a>.
        </p>
        <p>
          Then choose the modes of transportation and destinations you want
          to evaluate, and watch as the colors on the map change. Blue
          indicates high accessibility, while red indicates low accessibility.
          You can click a street segment to view the access scores for
          each mode of transportation to the selected destinations.
        </p>
        <p>
          To learn how future transportation projects may impact individuals
          and families, choose an access story from the menu below, or from
          the information menu in the top toolbar.
        </p>
        <ion-grid>
          <ion-row>
            <ion-col>
              <ion-button expand="block"
                  onClick={() => this.closeModal()}>
                <ion-icon name="map" slot="start"></ion-icon>
                {_t('as.intro.map')}
              </ion-button>
            </ion-col>
            <ion-col>
              <ion-button color="secondary" expand="block"
                  onClick={(e) => this.openPopover(e)}>
                <ion-icon name="people" slot="start"></ion-icon>
                {_t('as.intro.story')}
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </as-modal>
    ]);
  }
}
