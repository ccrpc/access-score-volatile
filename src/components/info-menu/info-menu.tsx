import { h, Component, Element } from '@stencil/core';
import { modalController } from '@ionic/core';
import { _t } from '../../i18n/i18n';


@Component({
  tag: 'as-info-menu'
})
export class InfoMenu {
  @Element() el: HTMLElement;

  closePopover() {
    this.el.closest('ion-popover').dismiss();
  }

  async openModal(component: string) {
    const modal = await modalController.create({
      component: component
    });

    this.closePopover();
    await modal.present();
  }

  render() {
    return (
      <ion-list>
        <ion-item button={true} onClick={() => this.openModal('as-intro')}>
          <ion-label>{_t('as.info-menu.intro')}</ion-label>
        </ion-item>
        <ion-item button={true}
            onClick={() => this.openModal('as-scenario-help')}>
          <ion-label>{_t('as.info-menu.scenario')}</ion-label>
        </ion-item>
        <ion-item button={true}
            onClick={() => this.openModal('as-destination-help')}>
          <ion-label>{_t('as.info-menu.destination')}</ion-label>
        </ion-item>
        <as-story-list></as-story-list>
      </ion-list>
    );
  }
}


