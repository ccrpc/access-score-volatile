import { h, Component, Prop } from '@stencil/core';
import { modalController } from '@ionic/core';


@Component({
  tag: 'as-help-button'
})
export class HelpButton {

  @Prop() buttonTitle: string;
  @Prop() component: string;

  async openModal() {
    const modal = await modalController.create({
      component: this.component
    });

    await modal.present();
  }

  render() {
    return ([
      <ion-button slot="end" fill="clear" color="medium"
          title={this.buttonTitle}
          onClick={() => this.openModal()}>
        <ion-icon slot="icon-only" name="help-circle"></ion-icon>
      </ion-button>
    ]);
  }
}
