import { h, Component } from '@stencil/core';
import { _t } from '../../i18n/i18n';
import { getDataUrl } from '../utils';


@Component({
  tag: 'as-destination-help'
})
export class DestinationHelp {

  render() {
    return ([
      <as-modal name={_t('as.info-menu.destination')}>
        <p>
          There are ten types of destinations in Access Score. While not
          a comprehensive list, these destinations represent some of the
          locations members of the community visit on a regular basis.
        </p>
        <rpc-table table-title="Destination Types"
          url={getDataUrl('destination-types.csv')}
          download={false}
        ></rpc-table>
        <p>
          The destinations are used to calculate the accessibility scores
          for each street segment based on the ease or difficulty of reaching
          the location using the transportation network. For destination types
          that have subtypes, the score is calculated for each subtype
          separately, and these subtype scores are averaged to get the
          destination score. This process reflects the reality that most
          households, for example, need access to both a library and a
          post office. 
        </p>
        <p>
          The scope for each destination type indicates which destination
          is used to calculate the score. For example, the most easily
          accessible park determines the score for the park destination type.
          For retail stores, however, households need access to a variety
          of different stores. Therefore, the fifth most easily accessible
          store is used to calculate the score for the retail stores
          destination type. Ease of access to a destination is based on both
          distance and level of service between the street segment and
          the destination.
        </p>
        <p>
          For the jobs destination type, the score is determined by the total
          number of jobs within a certain accessibility threshold. The
          threshold is based on the most difficult trip an average person would
          be willing to make using each mode of transportation.
        </p>
      </as-modal>
    ]);
  }
}
