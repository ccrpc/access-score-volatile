import { h, Component, Host, Listen, Prop, State } from '@stencil/core';
import { _t } from '../../i18n/i18n';
import { getToggles, getColor } from '../utils';


@Component({
  styleUrl: 'scores.css',
  tag: 'as-scores'
})
export class Scores {
  @State() scores: [string, string, number][] = [];

  @Prop() features: any[];

  componentWillLoad() {
    this.updateScores();
  }

  @Listen('asToggleChange', { target: 'body' })
  handleToggleChange() {
    this.updateScores();
  }

  updateScores() {
    let destinations = getToggles('destinations');
    let modes = getToggles('modes');
    let scores = [];
    let scoreValues = [];

    for (let mode of modes) {
      let score: number;
      if (!mode.enabled) {
        score = null;
      } else {
        let destScores = destinations
          .filter((dest) => dest.enabled)
          .map((dest) =>
            this.features[0].properties[`${mode.value}_${dest.value}`]);
        score = (destScores.length) ?
          destScores.reduce((a, b) => a + b) / destScores.length : null;
        scoreValues.push(score);
        score = Math.round(score);
        score = (isNaN(score)) ? null : score;
      }

      scores.push([_t(`as.app.modes.${mode.value}`), mode.icon, score]);
    }
    let combinedScore = (scoreValues.length > 0) ?
      Math.round(scoreValues.reduce((a, b) => a + b) / scoreValues.length) :
      null;
    combinedScore = (isNaN(combinedScore)) ? null : combinedScore;
    scores.push([_t(`as.app.combined`), 'cube', combinedScore]);
    this.scores = scores;
  }

  render() {
    let scenarios = document.querySelector('as-scenarios');
    let compare = (scenarios.scenario &&
      scenarios.scenario.difference && scenarios.compare);
    let rampName = (compare) ? 'difference' : 'absolute';
    
    let scores = this.scores.map(([label, icon, score]) =>
      <as-score label={label} icon={icon} compare={compare}
        value={score} color={getColor(rampName, score)}></as-score>);
    let heading: string = _t('as.scores.heading');
    let subheading: string;
    let props = this.features[0].properties;
    if (props.name) {
      heading = props.name;
      if (props.cross_name_start && props.cross_name_end)
        subheading = _t('as.scores.subheading', {
          start: props.cross_name_start.replace(/ /g, '\u00a0'),
          end: props.cross_name_end.replace(/ /g, '\u00a0')
        });
    }
    return (
      <Host id={`scores-segment-${props.segment_id}`}>
        <h2>{heading}</h2>
        {(subheading) ? <h3>{subheading}</h3> : null}
        <ion-list>
          {scores}
        </ion-list>
      </Host>
    );
  }
}
