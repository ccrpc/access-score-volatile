import { h, Component, Prop } from '@stencil/core';
import { modalController } from '@ionic/core';
import { _t } from '../../i18n/i18n';


@Component({
  styleUrl: 'app.css',
  tag: 'as-app'
})
export class App {
  @Prop() baseUrl: string;
  @Prop() geocodeBbox: string;
  @Prop() geocodeUrl: string;
  @Prop() latitude: number;
  @Prop() longitude: number;
  @Prop() maxzoom: number;
  @Prop() storyId: string;
  @Prop() zoom: number;

  async componentDidLoad() {
    let key = 'as.intro';
    let shown = localStorage.getItem(key) === 'true';
    if (!shown) {
      localStorage.setItem(key, 'true');
      this.showIntro();
    }
  }

  async showIntro() {
    const modal = await modalController.create({
      component: 'as-intro'
    });

    await modal.present();
  }
  
  render() {
    return (
      <gl-app label={_t('as.app.title')} menuLabel={_t('as.app.options')}>
        <as-info-button slot="end-buttons"></as-info-button>
        <gl-share-button slot="end-buttons"></gl-share-button>
        <gl-fullscreen slot="end-buttons"></gl-fullscreen>
        <gl-map longitude={this.longitude} latitude={this.latitude}
            zoom={this.zoom} maxzoom={this.maxzoom} hash="map">
          <as-style baseUrl={this.baseUrl}></as-style>
          <gl-style url="https://maps.ccrpc.org/basemaps/basic/style.json"
            basemap={true} enabled={true}></gl-style>
          <gl-address-search
            bbox={this.geocodeBbox}
            url={this.geocodeUrl}></gl-address-search>
        </gl-map>
        <as-legend></as-legend>
        <ion-list slot="menu">
          <slot />
        </ion-list>
        <gl-drawer drawerTitle={_t('as.app.stories')}>
          <gl-story-button action="previous" storyId={this.storyId}
            slot="toolbar-end-buttons"></gl-story-button>
          <gl-story-button action="next" storyId={this.storyId}
            slot="toolbar-end-buttons"></gl-story-button>
          <slot name="drawer"></slot>
        </gl-drawer>
      </gl-app>
    );
  }
}
