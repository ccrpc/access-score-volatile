import { h, Component } from '@stencil/core';
import { _t } from '../../i18n/i18n';
import { getDataUrl } from '../utils';


@Component({
  styleUrl: 'scenario-help.css',
  tag: 'as-scenario-help'
})
export class ScenarioHelp {

  render() {
    return ([
      <as-modal name={_t('as.info-menu.scenario')}>
        <h2 class="sr-only">Transportation Vision</h2>
        <div class='embed-container'>
          <iframe src="https://www.youtube-nocookie.com/embed/YeY1VC8UDpg"
            frameborder="0" allowFullScreen></iframe>
        </div>
        <p>
          Public input from <a href="https://ccrpc.gitlab.io/lrtp2045">Long
          Range Transportation Plan 2045</a> conveyed strong support for a set
          of overlapping ideas about the future of transportation:
        </p>
        <ul>
          <li>More bicycle and pedestrian infrastructure</li>
          <li>Shorter off-campus transit times</li>
          <li>A more environmentally-sustainable transportation system</li>
          <li>Limited sprawl</li>
          <li>Automated vehicles (AVs)</li>
          <li>High speed rail</li>
        </ul>
        <h2>Scenarios</h2>
        <p>Based on this vision for the future, CUUATS LRTP 2045 Steering
          Committee developed three future scenarios, in addition to the 2015
          baseline scenario:
        </p>
        <ul>
          <li>
            <strong>2015 Baseline:</strong> Based on 2015 data,
            it is intended to reflect current conditions.
          </li>
          <li>
            <strong>2045 Business-as-Usual:</strong> Forecasts how and where
            development will occur between 2015 and 2045 based on historic
            development trends. It is included in the comparison for reference
            but is not modeled in Access Score.
          </li>
          <li>
            <strong>2045 Preferred:</strong> Reflects ambitious implementation
            of bike and pedestrian recommendations in current plans, projected
            transit system changes, future environmental considerations and
            actions, and an emphasis on infill development.
          </li>
          <li>
            <strong>2045 Preferred + HSR:</strong> Incorporates the
            construction of a high speed rail (HSR) line to Chicago by the
            year 2040 into the Preferred 2045 scenario.
          </li>
        </ul>
        <h2>Population and Employment</h2>
        <p>
          Greater population and employment growth are projected for the
          2045 Preferred Scenario + High Speed
          Rail. <a href="https://www.midwesthsr.org/sites/default/files/studies/MHSRA_TranSystems_2012.pdf">
            A network benefits study
          </a> indicated that new households and jobs would be attracted to
          the area.
        </p>
        <rpc-table table-title="Population and Employment Projections"
          url={getDataUrl('scenario-population-employment.csv')}
          textAlignment="l,r"
          download={false}
        ></rpc-table>
        <h2>Land Use and Housing</h2>
        <p>
          While Business-as-Usual assumes ongoing development at the
          peripheries of municipal areas, the preferred scenarios implement
          building constraints, limiting new development to the existing
          municipal boundaries. This shift maximizes the utility of existing
          infrastructure and helps to preserve land for agricultural uses.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-land-use.csv')}
          type="horizontalBar"
          xLabel="Percent"
          stacked={true}
          source="CUUATS and Social Cost of Alternative Land Development Scenarios (SCALDS) model"
          chartTitle="Projected Land Use in Champaign County"
          download={false}></rpc-chart>
        <p>
          While Business-as-Usual predicts an increase of 2,021 single family
          homes, the preferred scenarios show much larger increases in
          multi-family housing units. The preferred scenario predicts 
          demolition and redevelopment of low-density residential parcels 
          to higher density multi-family uses. 
        </p>
        <rpc-chart
          url={getDataUrl('scenario-housing.csv')}
          type="horizontalBar"
          xLabel="Housing Units"
          xMin={0}
          source="CUUATS and Social Cost of Alternative Land Development Scenarios (SCALDS) model"
          chartTitle="Projected Total Housing Units in Champaign County"
          download={false}></rpc-chart>
        <h2>Travel Patterns</h2>
        <p>
          The total vehicle miles travelled (VMT) is projected to increase
          approximately 39 percent under the Business-As-Usual scenario. That
          increase is reduced under the preferred scenario as a result of an
          increased share of transit, bicycling, and walking trips due to
          improved and expanded infrastructure for those modes. With high speed
          rail, approximately 204,750 annual car trips are projected to be
          replaced with rail trips.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-vmt.csv')}
          type="horizontalBar"
          legend={false}
          xLabel="Vehicle Miles Traveled (Millions)"
          xMin={0}
          source="CUUATS and Champaign County Travel Demand Model (TDM)"
          chartTitle="Projected Vehicle Miles Traveled in the MPA"
          download={false}></rpc-chart>
        <rpc-chart
          url={getDataUrl('scenario-mode-share.csv')}
          type="horizontalBar"
          xLabel="Percent"
          xMin={0}
          xMax={100}
          stacked={true}
          source="CUUATS and Champaign County Travel Demand Model (TDM)"
          chartTitle="Projected Mode Share in the MPA"
          download={false}></rpc-chart>
        <h2>Energy and Emissions</h2>
        <p>
          Increasing density reduces the energy consumed by buildings, as well
          as transportation energy needs. Estimated solar production was
          removed from the predicted consumption. The preferred scenarios
          anticipated a greater adoption of electric vehicles, but the increase
          in solar production offsets this increased demand for electricity.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-energy.csv')}
          type="horizontalBar"
          xLabel="MMBtu"
          xMin={0}
          stacked={true}
          source="CUUATS and Social Cost of Alternative Land Development Scenarios (SCALDS) model"
          chartTitle="Projected Energy Consumption Per Capita in Champaign County"
          download={false}></rpc-chart>
        <p>
          A 39 percent electric vehicle fleet share is projected for the
          2045 Business-as-Usual scenario. The projected increase in active
          transportation mode share and an even higher share of electric
          vehicles under both 2045 Preferred Scenarios result in a decrease
          in the amount of emissions generated compared with 2015.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-emissions.csv')}
          type="horizontalBar"
          aspectRatio="1.25"
          xLabel="U.S. Ton"
          xMin={0}
          source="CUUATS and the MOtor Vehicle Emission Simulator (MOVES) model"
          chartTitle="Projected Emissions in the MPA"
          download={false}></rpc-chart>
        <h2>Health</h2>
        <p>
          People who routinely walk and bike are more likely to get the
          recommended levels of daily physical activity for good health,
          which can lower the risk of certain health conditions such as
          obesity, hypertension, and type II diabetes. Preliminary
          modeling suggests that bicycle and pedestrian
          accessibility improvements in the 2045 Preferred scenario could
          lead to 292 fewer patients (a 1.7% reduction) requiring treatment for
          these diseases per year compared to the 2015 Baseline scenario.
        </p>
        <rpc-chart
          url={getDataUrl('scenario-health.csv')}
          type="horizontalBar"
          xLabel="Patients Treated per Year"
          xMin={0}
          legend={false}
          aspectRatio={3}
          source="CUUATS Health Impact Assessment"
          chartTitle="Patients Treated for Selected Conditions in the UA"
          download={false}></rpc-chart>
      </as-modal>
    ]);
  }
}
