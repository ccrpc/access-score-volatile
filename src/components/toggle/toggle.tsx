import { h, Component, Element, Event, EventEmitter, Prop, Listen }
  from '@stencil/core';
import { _t } from '../../i18n/i18n';
import { hashManager } from '@ccrpc/hash-manager';


@Component({
  tag: 'as-toggle'
})
export class Toggle {
  @Element() el: HTMLAsToggleElement;

  @Prop() value: string;
  @Prop() icon: string;
  @Prop({mutable: true}) enabled: boolean;

  @Event() asToggleChange: EventEmitter;

  componentWillLoad() {
    let defaults = hashManager.init(...this.getHashInfo(true));
    this.enabled = this.inHashString(defaults);
  }

  @Listen('hashManagerChange', {target: 'window'})
  hashChanged(e: CustomEvent) {
    let type = this.el.closest('as-toggles').type;
    let inOld = this.inHashString(e.detail.old[type]);
    let inNew = this.inHashString(e.detail.new[type]);
    if (inOld !== inNew) this.enabled = inNew;
  }

  getHashInfo(all: boolean = false) : [string, string] {
    let parent = this.el.closest('as-toggles');
    let toggles = parent.querySelectorAll('as-toggle');
    let hashString = Array.from(toggles)
      .filter((t) => t.enabled || all)
      .map((t) => t.value)
      .join(',');
    return [parent.type, hashString];
  }

  inHashString(hs: string) {
    return (hs || '').split(',').indexOf(this.value) > -1;
  }

  changeHandler(e: CustomEvent) {
    this.enabled = e.detail.checked;
    this.asToggleChange.emit();
    hashManager.set(...this.getHashInfo());
  }

  render() {
    let type = this.el.closest('as-toggles').type;
    return (
      <ion-item>
        { (this.icon) ?
          <ion-icon name={this.icon} slot="start"></ion-icon> : null }
        <ion-label>{_t(`as.app.${type}.${this.value}`)}</ion-label>
        <ion-toggle slot="end" value={this.value} checked={this.enabled}
          color={(type == 'modes') ? 'primary' : 'secondary'}
          onIonChange={(e) => this.changeHandler(e)}></ion-toggle>
      </ion-item>
    );
  }
}
