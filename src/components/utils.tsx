import { getAssetPath } from '@stencil/core';


export function getToggles(type: string) : HTMLAsToggleElement[] {
  return Array.from(
    document.querySelectorAll(`as-toggles[type='${type}'] as-toggle`));
}

export const colorRamps = {
  absolute: [
    0, '#f50814',
    50, '#f5fa8a',
    100, '#030bf2'
  ],
  difference: [
    -10, '#f50814',
    0, '#eeeeee',
    10, '#030bf2'
  ]
};

function toHex(rgb: number[]) {
  let parts = rgb.map((v) => {
    let hex = Math.round(v).toString(16);
    return (hex.length == 1) ? `0${hex}` : hex;
  });
  return `#${parts.join('')}`;
}

function toRGB(hex: string) {
  let matches = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  matches.shift();
  return matches.map((hex) => parseInt(hex, 16));
}

export function getColor(rampName: string, value: number) {
  let ramp = colorRamps[rampName];
  for (let i=3; i<ramp.length; i+=2) {
    let startValue = ramp[i-3];
    let startColor = ramp[i-2];
    let endValue = ramp[i-1];
    let endColor = ramp[i];

    if (value <= startValue) return startColor;
    if (value >= endValue) continue;

    let pct = (value - startValue) / (endValue - startValue);
    let c0 = toRGB(startColor);
    let c1 = toRGB(endColor);
    return toHex([0, 1, 2].map((i) => c0[i] + (c1[i] - c0[i]) * pct));
  }
  return ramp[ramp.length -1];
}

export function getDataUrl(filename: string) {
  return getAssetPath(`../assets/data/${filename}`);
}
