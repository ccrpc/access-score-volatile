import { h, Component, Element, Prop } from '@stencil/core';
import { _t } from '../../i18n/i18n';


@Component({
  styleUrl: 'modal.css',
  tag: 'as-modal'
})
export class Modal {
  @Element() el: HTMLElement;

  @Prop() name: string;

  closeModal() {
    this.el.closest('ion-modal').dismiss();
  }

  render() {
    return ([
      <ion-header>
        <ion-toolbar>
          <ion-title>{this.name}</ion-title>
          <ion-buttons slot="end">
            <ion-button onClick={() => this.closeModal()}
                title={_t('as.static-modal.close')}>
              <ion-icon slot="icon-only" name="close"></ion-icon>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content class="ion-padding">
        <slot />
      </ion-content>
    ]);
  }
}
